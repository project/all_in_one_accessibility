# All in One Accessibility

## Description

This module provides few options to make website WCAG & ADA Compliance.

This module adds the scripts on all over the site. That script will
download additional script to provide ADA controls.

## CONFIGURATION

  - Configure user permissions in Administration » People » Permissions
    
      - Access All in One Accessibility
        
        Users in roles with "Add ADA Tool/Script all over the site"
        permission can add / remove the Tool/Script from entire website.

  - Update color of ADA widget as per website theme in settings on the
    Administer -> Configuration -> Development -> All in One
    Accessibility Page.

## MAINTAINERS

Current maintainers: * Rajesh Bhimani (Skynet Technologies) -
<https://www.drupal.org/user/3479403>

This project has been sponsored by: * Skynet Technologies Skynet
Technologies is ISO 9001:2015 & 27001:2013 certified company providing
end-to-end IT Services including Website Design & Development, ecommerce
shopping cart, SEO & Digital Marketing, ADA Website Design, Custom
Analytics, Mobile App, CRM, ERP & Custom Software Development for 19
years. We deliver our services to Start-Ups, SMEs, Corporates,
Government & Agencies.

Our headquarter is in Amelia, Ohio. Also, We have subsidiaries in the
USA (Nevada, Florida), India, and Australia. We are providing solutions
for small to large businesses, enterprises, corporations, web
development agencies, and firms. We believe in the customer-first
approach. We go above and beyond to meet the tailored needs of our
customers. Our primary aim is to serve our customers by allowing them to
do a successful and profitable business with us online. Visit
<https://www.skynettechnologies.com> for more information.
